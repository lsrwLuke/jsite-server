/*jshint esversion:6, node:true*/

const marked = require("marked");

module.exports = string => {
    return (
        marked(string)
            // table => Bootstrap table
            .replace(/<table/gu, '<table class="table table-hover"')
            // h1 => heading + hr
            .replace(/<\/h1/gu, "</h1><hr/")
            // table heading => no top border
            .replace(/<th>/gu, '<th class="border-top-0">')
            // heading => heading + margins
            .replace(
                /<h(?<heading>2|3|4|5|6) /gu,
                '<h$<heading> class="mt-4 mb-3" '
            )
    );
};
