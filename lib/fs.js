/*jshint esversion:6, node:true*/

const fs = require("fs");

module.exports = {
    createReadStream: path => {
        return fs.createReadStream(path);
    },
    mkdir: path => {
        return new Promise((resolve, reject) => {
            return fs.mkdir(path, e => (e ? reject(e) : resolve(false)));
        });
    },
    readFile: (path, encode = "utf8") => {
        return new Promise((resolve, reject) => {
            return fs.readFile(path, encode, (e, data) => {
                return e ? reject(e) : resolve(data);
            });
        });
    },
    readdir: path => {
        return new Promise((resolve, reject) => {
            return fs.readdir(path, (e, file) => {
                return e ? reject(e) : resolve(file);
            });
        });
    },
    stat: path => {
        return new Promise((resolve, reject) => {
            return fs.stat(path, (e, stats) => {
                return e ? reject(e) : resolve(stats);
            });
        });
    },
    writeFile: (path, data) => {
        return new Promise((resolve, reject) => {
            return fs.writeFile(path, data, e => (e ? reject(e) : resolve()));
        });
    }
};
