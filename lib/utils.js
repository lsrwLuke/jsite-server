/*jshint esversion:6, node:true*/

module.exports = {
    isPromise(object) {
        return Object.prototype.toString.call(object) === "[object Promise]";
    }
};
