# JSite Server

## Installation

### npm

```cmd
npm install jsite-server
```

## Usage

```js
const JSiteServer = require("jsite-server");

/**
 * Initiate Server
 */
let Server1 = new JSiteServer();

// Specify options, no callback
let Server2 = new JSiteServer({
    request: false
});

// Default options, with callback
let Server3 = new JSiteServer(function (error) {
    console.log(error ? error : "Started.");
});

// Specify options, with callback
let Server4 = new JSiteServer({
    request: false
}, function (error) {
    console.log(error ? error : "Started.")
});

/**
 * Start Listening
 */
Server1.listen();

// Specify port
Server2.listen(8080);

// Specify port, with delay
Server3.listen(80, 2000);
```

## Events

### From [http.Server](https://nodejs.org/api/http.html#http_class_http_server)

-   Changes:
    -   [request](https://nodejs.org/api/http.html#http_event_request) - only emit if "request" option is `false`

-   No Changes:
    -   [checkContinue](https://nodejs.org/api/http.html#http_event_checkcontinue)
    -   [checkExpectation](https://nodejs.org/api/http.html#http_event_checkexpectation)
    -   [clientError](https://nodejs.org/api/http.html#http_event_clienterror)
    -   [close](https://nodejs.org/api/http.html#http_event_close)
    -   [connect](https://nodejs.org/api/http.html#http_event_connect_1)
    -   [connection](https://nodejs.org/api/http.html#http_event_connection)
    -   [upgrade](https://nodejs.org/api/http.html#http_event_upgrade_1)

### New

-   complete: Emitted each time the request output is finished (see "output")

## Documentation

### Options

| Name      | Type (default)                                                                                                           | Description                           |
| --------- | ------------------------------------------------------------------------------------------------------------------------ | ------------------------------------- |
| request   | [Boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean) (`true`)             | Handle the "request" event            |
| index     | [Boolean](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean) (`true`)             | Allow index.json files                |
| directory | [Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array) (see "Directory Files")  | Directory files, in order of priority |
| output    | [Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions) (see "Request Output") / `false` | Function for request output           |
| error     | [Function](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions) (_none_)                         | Handler for status codes              |
| before    | _Any_                                                                                                                    | Handler for pre-serving               |

#### Directory Files (default)

`["index.js", "index.html", "index.json"]`

"index.json" will only be used when in "index" mode.

#### Request Output (default)

Output is called within an Array.map(), the following variables are available:

-   Argument 1 (`p` in default) - URL (or file) path, with trailing slash.
-   Argument 2 (`p_i` in default) - Item index within the Array being mapped.
-   `request` - [http.IncomingMessage](https://nodejs.org/api/http.html#http_class_http_incomingmessage) for the current request.
-   `info` - Object with "headers" ([Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)), "data" (Readable / String), "status" (Number) properties.

Default output function:

```js
function(p, p_i) {
    if (p_i === 0) p += ` (${new Date().toISOString()})`;
    if (p_i === request.url.prev.length - 1) p += ` [${info.status}]`;
    return "\t" + p.replace(/\\/g, "/");
}
```

#### Error Handler (example)

Handler used for HTTP status code pages, defaults to just code and description.

Example status code handler:

```js
function(status) {
    return new Promise((resolve, reject) => {
        let file = path.join(__dirname, "public", status + ".html");
        return fs.stat(file, e => {
            if (e) return reject(status);
            return resolve({
                data: fs.createReadStream(file),
                status
            });
        });
    });
}
```

#### "Before" Handler (example)

You can process/provide information to the request using the "before" handler.
This could be used for logging, or adding global variables to each request handler.
You can provide this is any data type, functions will be executed (including Promises).

The returned value of the "before" handler will be stored into `request.extra`.

Example before handler:

```js
function(request) {
    return new Promise((resolve, reject) => {
        return resolve({
            foo: "bar"
        });
    });
}
```
