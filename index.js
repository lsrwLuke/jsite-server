/*jshint esversion:6, node:true*/

/**
 * Node.js Modules
 */
const { EventEmitter } = require("events"); // for emitting events
const { createServer, STATUS_CODES } = require("http"); // for creating server
const { Readable } = require("stream"); // for handling streams
const { parse } = require("url"); // for parsing initial request URL
const path = require("path"); // for building/manipulating file paths

/**
 * NPM Packages
 */
const { lookup } = require("mime-types"); // for lookup of file mime types
const qs = require("qs").parse; // for parsing request data
const http = require("http-status");

/**
 * Custom Libraries
 */
const fs = require("./lib/fs"); // for implementing "fs" with promises
const marked = require("./lib/marked"); // for setting up initial files
const { isPromise } = require("./lib/utils"); // for random helpful utilities

/**
 * Constant variables
 */
const DEFAULT_VARS = {
    listen: {
        delay: 0,
        port: 80
    }
};

/**
 * Identify if "code" is in same range as "range"
 * Turns "range" of 300, 301, 302, etc. into 300
 *
 * @param {string|number} code - Code to check (e.g. 301)
 * @param {string|number} range - Range to check (e.g. 300)
 * @returns {bool} Whether "code" in same HTTP range as "range"
 */
function inHTTPRange(code, range) {
    /**
     * Reduce range to lowest hundred
     */
    range = Math.floor(range / 100) * 100;

    return code >= range && code < range + 100;
}

/**
 * Error handling of server errors
 *
 * @param {string|number|object} data Status code/error object
 * @returns {object} Data and status code
 */
function errorHandle(data) {
    /**
     * In the event of an error, return the error
     * Status is set as the error (if numeric), otherwise server error
     */
    if (data instanceof Error) data = data.message;
    if (!(typeof data === "number" || typeof data === "string")) return data;

    let status = data;
    if (Object.prototype.hasOwnProperty.call(STATUS_CODES, data)) {
        data += ` - ${STATUS_CODES[data]}`;
    } else {
        status = http.INTERNAL_SERVER_ERROR;
    }

    return {
        data,
        status
    };
}

const options_default = {
    directory: ["index.js", "index.html", "index.json"]
};

module.exports = class JSiteServer extends EventEmitter {
    /**
     * JSiteServer constructor
     *
     * @author lsrwLuke
     * @version 1.0
     * @constructor
     * @param {object} options - Object of server options
     * @param {?function} callback - Callback for server startup
     */
    constructor(options, callback) {
        /**
         * super() is called to initialize EventEmitter class
         */
        super();

        /**
         * this.abs stores the absolute position of "public" directory
         */
        this.abs = path.resolve(process.mainModule.filename, "..");
        if (Object.prototype.hasOwnProperty.call(process.env, "PWD")) {
            this.abs = process.env.PWD;
        }
        this.abs = path.join(this.abs, "public");

        /**
         * this.options stores the server-wide options
         */
        if (typeof options === "function") callback = options;

        this.options = Object.assign(
            options_default,
            typeof options === "object" ? options : {}
        );

        /**
         * this.Server is the base HTTP server
         */
        this.Server = createServer();

        /**
         * Default handler for re-emitting events from HTTP server
         */
        [
            "checkContinue",
            "checkExpectation",
            "clientError",
            "close",
            "connect",
            "connection",
            "upgrade"
        ].forEach(event => {
            this.Server.on(event, (...args) => this.emit(event, ...args));
        });

        /**
         * Handler for incoming requests
         */
        this.Server.on("request", (request, res) => {
            /**
             * Convert the method to lowercase for easier handling
             */
            request.method = request.method.toLowerCase();

            /**
             * Parse the inbound URL into standard format
             */
            request.url = this.getURL(request.url);

            /**
             * Prepare for request data object
             *
             * "get" always exists, contains GET parameters
             * request.method will also be used for body content
             */
            request.data = {};
            request.data[request.method] = "";
            request.data.get = {};
            if (request.url.search) {
                request.data.get = qs(request.url.search.substr(1));
            }

            /**
             * Append data to body when recieved
             */
            request.on("data", data => (request.data[request.method] += data));

            /**
             * End of request, begin processing
             */
            request.on("end", () => {
                /**
                 * Parse request data into object
                 */
                try {
                    request.data[request.method] = JSON.parse(
                        request.data[request.method]
                    );
                } catch (ignore) {
                    request.data[request.method] = qs(
                        request.data[request.method]
                    );
                }

                /**
                 * Allow custom handling of inbound request
                 */
                if (!this.getOption("request", true)) {
                    return this.emit("request", request, res);
                }

                return this.before(request)
                    .then(() => this.find(request))
                    .catch(this.getOption("error", errorHandle))
                    .catch(errorHandle)
                    .then(info => {
                        /**
                         * Set default properties if missing from output
                         */
                        info = Object.assign(
                            {
                                data: "",
                                status: http.OK
                            },
                            info
                        );

                        /**
                         * Attempt to JSON parse
                         */
                        try {
                            info.data = JSON.parse(info.data);
                        } catch (ignore) {
                            // ignore parsing errors
                        }

                        /**
                         * Handle different data types
                         */
                        if (typeof info.data === "function") {
                            info.data = info.data(request);
                        }

                        /**
                         * Set headers from information
                         */
                        this.setHeaders(info);

                        res.writeHead(info.status, info.headers);
                        if (info.data instanceof Readable) {
                            info.data.pipe(res);
                        } else {
                            if (typeof info.data === "object") {
                                info.data = JSON.stringify(info.data);
                            }
                            res.end(`${info.data} `);
                        }

                        return this.complete(request, info);
                    });
            });
        });

        /**
         * Setup server for initialisation
         */
        this.setup()
            .catch(e => e)
            .then(e => {
                if (typeof callback === "function") return callback(e);

                console.log(`
    Server Started - ${new Date().toISOString()}
`);
            });
    }

    before(request, extra) {
        return new Promise((resolve, reject) => {
            if (extra === undefined) {
                extra = this.getOption("before", false);
                if (typeof extra === "function") {
                    extra = extra(request);
                    if (isPromise(extra)) {
                        return extra
                            .then(out => this.before(request, out || null))
                            .then(resolve)
                            .catch(reject);
                    }
                }
            }

            request.extra = extra;

            return resolve(request);
        });
    }

    /**
     * Output request history, then emit after event
     *
     * @param {http.IncomingMessage} req - Inbound server request
     * @param {object} info - Built response information
     * @returns {boolean} Result from EventEmitter.emit
     */
    complete(req, info) {
        /**
         * Output request history
         */
        let output = this.getOption("output", (file, file_i) => {
            if (file_i === 0) file += ` (${new Date().toISOString()})`;
            if (file_i === req.url.prev.length - 1) file += ` [${info.status}]`;

            return `    ${file.replace(/\\/gu, "/")}`;
        });
        if (output && typeof output === "function") {
            console.log(`${req.url.prev.map(output).join("\n")}
`);
        }

        return this.emit("complete", req, info);
    }

    /**
     * Establish "public" directory on server startup
     *
     * @author lsrwLuke
     * @version 1.0
     * @returns {Promise} Pending promise for establishing public directory
     */
    setupPublic() {
        return new Promise((resolve, reject) => {
            /**
             * Read from the "lib/html/public.html" file
             */
            let routes = [
                [__dirname, "lib", "html", "public.html"],
                [__dirname, "README.md"]
            ];

            /**
             * Check if the public directory exists
             */
            return fs
                .stat(this.abs)
                .catch(() => fs.mkdir(this.abs))
                .then(stats => {
                    if (stats) return resolve();

                    return Promise.all(
                        routes.map(route => {
                            route = path.join(...route);

                            return fs.readFile(route);
                        })
                    );
                })
                .then(html => {
                    if (!html) return resolve();

                    let md = ".md";
                    let text = "";
                    routes.forEach((route, route_i) => {
                        if (
                            route[route.length - 1].substr(md.length * -1) ===
                            md
                        ) {
                            text = text.replace(
                                new RegExp(
                                    `<!-- ${route[route.length - 1]} -->`,
                                    "gu"
                                ),
                                marked(html[route_i])
                            );
                        } else {
                            text += html[route_i];
                        }
                    });

                    return fs.writeFile(
                        path.join(this.abs, "index.html"),
                        text
                    );
                })
                .then(resolve)
                .catch(reject);
        });
    }

    /**
     * Establish server on startup
     *
     * @author lsrwLuke
     * @version 1.0
     * @returns {Promise} Pending promise for server startup
     */
    setup() {
        return new Promise((resolve, reject) => {
            return this.setupPublic()
                .then(resolve)
                .catch(reject);
        });
    }

    /**
     * Format the headers for client response
     *
     * @author lsrwLuke
     * @version 1.0
     * @param {object} info - Near-final response object
     * @returns {object} info - Near-final response object (with headers)
     */
    setHeaders(info) {
        /**
         * Set empty headers object, if missing
         */
        if (!Object.prototype.hasOwnProperty.call(info, "headers")) {
            info.headers = {};
        }

        if (info.data instanceof Readable) {
            /**
             * If data is a Readable, get Content-Type from path
             * Uses the "mime" package to get mime type of file
             */
            if (
                !Object.prototype.hasOwnProperty.call(
                    info.headers,
                    "Content-Type"
                ) &&
                Object.prototype.hasOwnProperty.call(info.data, "path")
            ) {
                info.headers["Content-Type"] = `${lookup(
                    info.data.path
                )}; charset=${info.data._readableState.defaultEncoding}`;
            }
        } else if (typeof info.data === "object") {
            /**
             * Set the Content-Type to application/json if object
             */
            info.headers["Content-Type"] = "application/json";
        }

        /**
         * If it's a redirect, there's no "Location" header - set from data
         */
        if (
            inHTTPRange(info.status, http.MOVED_PERMANENTLY) &&
            !Object.prototype.hasOwnProperty.call(info.headers, "Location")
        ) {
            info.headers.Location = info.data;
        }

        /**
         * If there's still no Content-Type, default to text/html
         */
        if (
            !Object.prototype.hasOwnProperty.call(info.headers, "Content-Type")
        ) {
            info.headers["Content-Type"] = "text/html";
        }
        /**
         * If there's no charset on Content-Type, set uf8
         */
        if (info.headers["Content-Type"].indexOf("; charset=") === -1) {
            info.headers["Content-Type"] += "; charset=utf8";
        }

        info = {
            data: info.data,
            headers: info.headers,
            status: info.status
        };

        return info;
    }

    /**
     * Retrieves server-wide option value
     *
     * @author lsrwLuke
     * @version 1.0
     * @param {string} name - Name of the option to retrieve
     * @param {*} value - Default value to return if not found
     * @returns {*} Value from option, or "value" variable
     */
    getOption(name, value) {
        if (Object.prototype.hasOwnProperty.call(this.options, name)) {
            return this.options[name];
        }

        return value;
    }

    /**
     * Allow external calls of HTTP server listen method
     *
     * @author lsrwLuke
     * @version 1.0
     * @param {number} [port=80] - Port number to listen on
     * @param {number} [delay=0] - Number of milliseconds to delay listen for
     * @returns {void}
     */
    listen(port = DEFAULT_VARS.listen.port, delay = DEFAULT_VARS.listen.delay) {
        setTimeout(
            () => {
                this.Server.listen(
                    typeof port === "number" ? port : DEFAULT_VARS.listen.port
                );
            },
            typeof delay === "number" ? delay : DEFAULT_VARS.listen.delay
        );
    }

    /**
     * Parse URL into standard object format
     *
     * @author lsrwLuke
     * @version 1.0
     * @param {string/object} file - URL string/object to reformat
     * @param {boolean} do_hop - Whether to move to parent directory
     * @param {string} ending - Whether to append string to end of URL
     * @param {boolean} is_index - Whether to set "index" mode enabled
     * @returns {object} URL object
     */
    getURL(file, do_hop = false, ending = "", is_index = null) {
        /**
         * Parse the initial URL using "url" Node.js module
         */
        if (typeof file === "string") file = parse(file, true);

        file.pathname = file.pathname.replace(/\//gu, path.sep);

        /**
         * Default origin and index if missing
         */
        file = Object.assign(
            {
                index: false,
                origin: file.pathname
            },
            file
        );

        if (typeof is_index === "boolean") file.index = is_index;

        /**
         * If do_hop is set, move the request one level up
         */
        if (do_hop) {
            file.pathname = file.pathname.substr(
                0,
                file.pathname.lastIndexOf(path.sep)
            );
        }

        /**
         * Append new ending to request, if provided
         */
        if (ending) file.pathname = path.join(file.pathname, ending);

        /**
         * If the request path doesn't start with a path seperator, add one
         */
        if (file.pathname.substr(0, 1) !== path.sep) {
            file.pathname = path.sep + file.pathname;
        }

        /**
         * Build URL information from request string, with absolute position
         * "end" is used to inspect the end of the request for specific files
         * "diff" is used to see the difference between current + request
         */
        file.abs = path.join(this.abs, file.pathname);
        file.end = file.abs.substr(file.abs.lastIndexOf(path.sep) + 1);
        file.diff = file.origin
            .substr(file.pathname.lastIndexOf(path.sep) + 1)
            .replace(/\\/gu, "/");

        /**
         * Store the previous requests, and whether or not we've seen it before
         */
        if (!Object.prototype.hasOwnProperty.call(file, "prev")) file.prev = [];
        file.isLoop = file.prev.indexOf(file.pathname) > -1;
        file.prev.push(file.pathname);

        return file;
    }

    /**
     * Find most relevant file to serve to the client
     *
     * @author lsrwLuke
     * @version 1.0
     * @param {http.IncomingMessage} req - Inbound server request
     * @returns {Promise} Pending promise for finding return file
     */
    find(req) {
        return new Promise((resolve, reject) => {
            /**
             * If the requested file is one of the "directory" files, redirect
             * This means that the requests shouldn't end in "index.html", etc.
             */
            if (
                this.getOption("directory", []).indexOf(req.url.end) > -1 &&
                req.url.origin.substr(req.url.end.length * -1) === req.url.end
            ) {
                return resolve({ data: ".", status: http.MOVED_PERMANENTLY });
            }

            /**
             * Run fs.stat on the absolute request, check if it exists
             */
            return fs
                .stat(req.url.abs)
                .then(stats => {
                    if (stats.isFile()) {
                        /**
                         * If the request is "index.js" or "index.json" file
                         * Run through this.getIndex to parse/serve
                         */
                        if (
                            req.url.end === "index.js" ||
                            req.url.end === "index.json"
                        ) {
                            return this.getIndex(req).catch(reject);
                        }

                        /**
                         * Read the file stream directly
                         */
                        return {
                            data: fs.createReadStream(req.url.abs),
                            headers: {
                                "Last-Modified": stats.mtime
                            }
                        };
                    }

                    if (stats.isDirectory()) {
                        /**
                         * If the request is a directory, scan for files
                         */
                        return fs
                            .readdir(req.url.abs)
                            .then(files => {
                                if (req.url.index) {
                                    /**
                                     * If server index mode, only index.json
                                     */
                                    if (files.indexOf("index.json") > -1) {
                                        files = "index.json";
                                    }
                                } else {
                                    /**
                                     * Scan for "directory" files, in order
                                     */
                                    this.getOption("directory", []).some(
                                        file => {
                                            if (
                                                file !== "index.json" &&
                                                files.indexOf(file) > -1
                                            ) {
                                                files = file;

                                                return true;
                                            }

                                            return false;
                                        }
                                    );
                                }

                                /**
                                 * If unable to find a file, reject with 404
                                 */
                                if (Array.isArray(files)) {
                                    return reject(http.NOT_FOUND);
                                }

                                /**
                                 * Rebuild URL from path changes
                                 */
                                req.url = this.getURL(req.url, false, files);

                                return this.find(req).catch(reject);
                            })
                            .then(resolve)
                            .catch(reject);
                    }

                    /**
                     * Requested stat wasn't file or directory
                     */
                    return reject("Invalid Statistics");
                })
                .catch(() => {
                    /**
                     * Unable to find file, reject 404 if index not allowed
                     */
                    if (!this.getOption("index", false)) {
                        return reject(http.NOT_FOUND);
                    }

                    /**
                     * If in index mode, move one level up
                     */
                    req.url = this.getURL(req.url, true, "", true);

                    return this.find(req);
                })
                .then(resolve)
                .catch(reject);
        });
    }

    /**
     * Retrieve the index.json file for path
     *
     * @author lsrwLuke
     * @version 1.0
     * @param {http.IncomingMessage} request - Inbound server request
     * @returns {Promise} Pending promise to get index.json file
     */
    getIndexJSON(request) {
        return new Promise((resolve, reject) => {
            /**
             * If the end file isn't "index.json", reject
             */
            if (request.url.end !== "index.json") return reject();

            /**
             * If we've seen this index.json file before, redirect properly
             */
            if (request.url.isLoop) return reject(http.LOOP_DETECTED);

            return fs
                .readFile(request.url.abs)
                .then(routes => {
                    /**
                     * Attempt to parse route file
                     */
                    try {
                        routes = JSON.parse(routes);
                    } catch (ignore) {
                        return reject();
                    }

                    if (typeof routes === "string") {
                        routes = {
                            file: routes
                        };
                    }

                    this.parseIndexJSON(request, routes);

                    return this.find(request);
                })
                .then(resolve)
                .catch(reject);
        });
    }

    /**
     * Parse index.json instructions for redirect/write
     *
     * @author lsrwLuke
     * @version 1.0
     * @param {http.IncomingMessage} request - Inbound Server Request
     * @param {object} paths - Parsed index.json instructions
     * @returns {object} Parsed index.json file
     */
    parseIndexJSON(request, paths) {
        if (typeof paths !== "object") return {};

        let match;
        let match_original;

        if (
            /**
             * Loop through index.json request object
             */
            !Object.keys(paths).some(reg => {
                /**
                 * match_original is the regex for this path
                 */
                match_original = new RegExp(`^${reg}`, "iu");
                /**
                 * match is the amount that matches the match_original
                 */
                match = match_original.exec(request.url.diff);

                /**
                 * If the regex match isn't exact to the diff
                 */
                if (!(match && match[0] === request.url.diff)) return false;

                /**
                 * If the json has a "file" property, allow change
                 */
                if (Object.prototype.hasOwnProperty.call(paths[reg], "file")) {
                    this.setPathname(request, paths[reg].file);
                } else {
                    /**
                     * If no "file", allow same directory scan again
                     */
                    request.url = this.getURL(request.url, true, "", false);
                }

                /**
                 * If "data" property provided in json
                 */
                if (Object.prototype.hasOwnProperty.call(paths[reg], "data")) {
                    /**
                     * Loop through data, replace regex matches in query data
                     */
                    Object.keys(paths[reg].data).forEach(key => {
                        request.data.get[key] = request.url.diff.replace(
                            match_original,
                            paths[reg].data[key]
                        );
                    });
                }

                return true;
            })
        ) {
            /**
             * Unable to find a match, jump up a level
             */
            request.url = this.getURL(request.url, true);
        }
    }

    /**
     * Retrieve/handle "index.js" file results
     *
     * @author lsrwLuke
     * @version 1.0
     * @param {http.IncomingMessage} request - Inbound server request
     * @param {string/object/function/Promise} handle - Resulting index output
     * @returns {Promise} - Pending promise on the index.js file
     */
    getIndex(request, handle) {
        /**
         * If the reuqest is index.json, do getIndexJSON instead
         */
        if (request.url.end === "index.json") return this.getIndexJSON(request);

        return new Promise((resolve, reject) => {
            /**
             * Delete the cached request, if any provided
             */
            if (handle === undefined) {
                delete require.cache[require.resolve(request.url.abs)];
                handle = require(request.url.abs);
                if (typeof handle === "function") handle = handle(request);
            }

            if (isPromise(handle)) {
                /**
                 * If handle is promise, wait for it to execute properly
                 */
                return handle
                    .then(result => this.getIndex(request, result))
                    .then(resolve)
                    .catch(reject);
            }

            if (handle) {
                /**
                 * If handle is a string, use as pathname
                 */
                if (typeof handle === "string") handle = { pathname: handle };

                /**
                 * If handle isn't object, use as "data" property
                 */
                if (
                    typeof handle !== "object" ||
                    Array.isArray(handle) ||
                    handle === null
                ) {
                    handle = { data: handle };
                }

                /**
                 * If handle has pathname, redirect pathname
                 */
                if (Object.prototype.hasOwnProperty.call(handle, "pathname")) {
                    if (handle.pathname.indexOf(this.abs) > -1) {
                        handle.pathname = handle.pathname.replace(this.abs, "");
                    }
                    this.setPathname(request, handle.pathname);
                    handle = this.find(request);
                }

                return resolve(handle);
            }
        });
    }

    /**
     * Set the new request pathname string
     *
     * @param {http.IncomingMessage} request - Inbound server request
     * @param {string} pathname - New pathname to use
     * @returns {object} Request URL object
     */
    setPathname(request, pathname) {
        if (typeof pathname !== "string") return true;
        if (pathname.substr(0, 1) === "/" || pathname.substr(0, 1) === "\\") {
            request.url.pathname = pathname;
            request.url = this.getURL(request.url);
        } else {
            request.url = this.getURL(request.url, true, pathname);
        }

        return request;
    }
};
