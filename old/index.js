/*jslint this single long white node*/

/**
 * Require Node.js core modules
 * http - for operating the HTTP Web Server (https://nodejs.org/api/http.html)
 * url - for parsing URL request paths (https://nodejs.org/api/url.html)
 * path - for parsing and building paths (https://nodejs.org/api/path.html)
 */
const http = require("http");
const url = require("url");
const path = require("path");

/**
 * Require NPM packages
 * qs - for parsing request query strings (https://www.npmjs.com/package/qs)
 */
const qs = require("qs");

/**
 * Require internal modules
 * sites - for handling multi-site support
 * serve - for serving responses through file, directory, data
 */
const sites = require("./lib/sites");
const serve = require("./lib/serve");

/**
 * jsite Server
 *
 * @param {String} base Directory name for public root
 */
module.exports = function(config) {
    "use strict";

    let self = this;

    this.config = Object.assign(
        {
            output: true,
            flags: {},
            extra: {},
            base: ""
        },
        config
    );
    this.callbacks = {};

    /**
     * Set base from config
     */
    this.base = process.argv[1] + path.sep;
    if (process.env.hasOwnProperty("INIT_CWD")) {
        this.base = path.join(process.env.INIT_CWD, "test");
    }
    if (typeof this.config.base !== "string") this.config.base = "";
    this.base += this.config.base;
    if (this.base.substr(-1) !== path.sep) this.base += path.sep;

    this.output = (origin, arg) => {
        if (self.config.hasOwnProperty("output") && self.config.output) {
            /**
             * Prepare generic log section
             */
            let log = {
                request: " ".repeat(arg.request.method.length + 1),
                properties: [origin]
            };
            if (origin.substr(0, 3) === "ssi") {
                log.request += arg.match[5].substr(
                    arg.request.config.base.length
                );
            } else if (arg.request.url.hasOwnProperty("file")) {
                log.request += arg.request.url.file.substr(
                    arg.request.config.base.length
                );
            } else {
                log.request += arg.request.url.pathname;
            }
            if (!log.request.trim().length) log.request += "/";
            if (arg.hasOwnProperty("status")) {
                log.properties[0] += " " + arg.status;
            }

            /**
             * Origin-specific logging
             */
            switch (origin) {
                case "directory":
                    if (arg.index_only) log.properties.push("index");
                    break;

                case "data":
                    if (
                        arg.request.config.flags.hasOwnProperty("ssi") &&
                        arg.request.config.flags.ssi
                    ) {
                        log.properties.push("ssi");
                    }
                    break;

                case "request":
                    console.log(
                        "\n" +
                            `${arg.request.method.toUpperCase()} ${
                                arg.request.url.pathname
                            }`
                    );
                    return true;
            }

            console.log(
                `${log.request.replace(/\\/g, "/")} [${log.properties.join(
                    ", "
                )}]`
            );
        }
    };

    /**
     * Awaits and parses the request body
     *
     * @param {Object} request Instance of HTTP.IncomingMessage
     */
    this.getData = request => {
        return new Promise((resolve, reject) => {
            /**
             * Reject if missing request
             */
            if (!request) return reject("Missing request");

            /**
             * Build data from GET request
             */
            let data = {
                get: qs.parse(request.url.search, {
                    ignoreQueryPrefix: true
                })
            };

            /**
             * Await request body, if Content-Length > 0
             */
            if (
                request.headers.hasOwnProperty("content-length") &&
                request.headers["content-length"]
            ) {
                return request
                    .on("data", buffer => {
                        if (!data.hasOwnProperty(request.method)) {
                            data[request.method] = "";
                        }
                        data[request.method] += buffer;
                    })
                    .on("end", function() {
                        /**
                         * Attempt to parse request body as JSON
                         * Otherwise use the qs.parse function
                         */
                        try {
                            data[request.method] = JSON.parse(
                                data[request.method]
                            );
                        } catch (ignore) {
                            data[request.method] = qs.parse(
                                data[request.method]
                            );
                        }
                        return resolve(data);
                    });
            }

            return resolve(data);
        });
    };

    /**
     * Create the HTTP Web Server
     */
    this.Server = http.createServer();

    /**
     * Setup event "listening" - on server startup
     */
    this.Server.on("listening", function() {
        if (self.config.output) {
            /**
             * Log time/date to console when the server has started
             */
            let address = self.Server.address().address;
            if (address === "::") {
                address = "localhost";
            }

            console.log(
                `${new Date()} -   Server: ${address}:${
                    self.Server.address().port
                } (${Object.keys(self.config.flags).join(", ")})`
            );
        }

        /**
         * Use the listening callback, if set
         */
        if (
            self.callbacks.hasOwnProperty("listening") &&
            typeof self.callbacks.listening === "function"
        ) {
            return self.callbacks.listening();
        }
    });

    /**
     * Setup event "request" - on server request
     */
    this.Server.on("request", (request, response) => {
        /**
         * Prepare request method, trim and lowercase
         */
        request.method = request.method.trim().toLowerCase();
        request.log = [];
        request.output = self.output;

        /**
         * Redirect the request if,
         * - Request method is "GET"
         * - Request is not a file
         * - Request does not end in a slash
         */
        request.url = url.parse(request.url, true);
        if (
            request.url.pathname
                .substr(request.url.pathname.lastIndexOf("/"))
                .indexOf(".") === -1 &&
            request.method === "get" &&
            request.url.pathname.substr(-1) !== "/"
        ) {
            return serve.redirect(
                request,
                response,
                307,
                request.url.pathname + "/"
            );
        }

        /**
         * Await request body data, if needed
         */
        return self
            .getData(request)
            .then(data => {
                request.data = data;
                return sites.getSite(request, self.config);
            })
            .then(site => {
                /**
                 * Re-store config for caching
                 */
                self.config = site.config;

                /**
                 * Build request.config, from:
                 * - existing Server config
                 * - current request site
                 *
                 * Ensure config has "flags" object
                 */
                request.config = Object.assign(
                    {
                        flags: self.config.flags,
                        base: self.base + site.site.directory
                    },
                    site.site
                );
                request.extra = self.config.extra;

                request.url.file = request.config.base + request.url.pathname;
                request.url.origin = request.url.file;

                self.output("request", { request });

                /**
                 * If the request is a file, use serve.file
                 */
                if (
                    request.url.file
                        .substr(request.url.file.lastIndexOf("/"))
                        .indexOf(".") > -1
                ) {
                    return serve.file(request, response);
                }
                return serve.directory(request, response);
            })
            .catch(error => {
                console.log(error);
            });
    });

    /**
     * Start server listening
     *
     * Configuration options:
     * "port" Port to listen on (default: 80)
     * "delay": Startup delay in MS (default: 0)
     * "callback": Callback for "listening"
     *
     * @param {Object} [config] Server listen configuration
     */
    this.listen = config => {
        /**
         * Allow config to substitute callback
         */
        if (typeof config === "function") {
            config = {
                callback: config
            };
        }

        config = Object.assign(
            {
                extra: {},
                port: 80,
                delay: 0,
                callback: null
            },
            config
        );

        /**
         * Set server "extra" variables
         */
        if (typeof config.extra === "object") self.config.extra = config.extra;

        /**
         * Set server "listening" callback
         */
        delete self.callbacks.listening;
        if (typeof config.callback === "function") {
            self.callbacks.listening = config.callback;
        }

        /**
         * Begin server startup after delay
         */
        setTimeout(function() {
            /**
             * Retrieve the multi-site config
             */
            return sites
                .getConfig(self.base)
                .then(site_config => {
                    self.config = Object.assign(self.config, site_config);
                    self.Server.listen(config.port);
                })
                .catch(error => {
                    console.log(error);
                });
        }, config.delay);
    };

    /**
     * Shut down the server
     */
    this.close = callback => {
        self.Server.close(callback);
    };
};
