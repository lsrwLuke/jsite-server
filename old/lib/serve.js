/*jslint this single long white node*/

/**
 * Require Node.js core modules
 * fs - for utilising the server file system (https://nodejs.org/api/fs.html)
 */
const fs = require("fs");
const path = require("path");

/**
 * Require NPM packages
 * mime - for returning Content-Type headers (https://www.npmjs.com/package/mime-types)
 */
const mime = require("mime-types");

/**
 * Require internal modules
 * route - for handling index.json route files
 * ssi - for adding support for Server Side Includes
 */
const route = require("./route");
const ssi = require("./ssi");

/**
 * Handle index.js file responses
 *
 * @param {Object} request Instance of HTTP.IncomingMessage
 * @param {Object} response Instance of HTTP.ServerResponse
 * @param {*} handle Handler of type Promise, Object, String, etc.
 */
function indexHandler(request, response, handle) {
    "use strict";

    /**
     * Handler returned a Promise, wait for this to .then/.catch
     * If this is successful, we recursively call indexHandler again
     * If this is unsuccessful, we call a status 500 page instead
     */
    if (Object.prototype.toString.call(handle) === "[object Promise]") {
        return handle
            .then(function(handle) {
                return indexHandler(request, response, handle);
            })
            .catch(function(error) {
                if (error) {
                    console.log(error);
                }

                return module.exports.status(request, response, 500);
            });
    }

    /**
     * Convert non-object handler responses
     * Strings are converted into file requests
     * Otherwise, the handler is completely ignored
     */
    if (typeof handle !== "object") {
        if (typeof handle === "string" && handle.length) {
            handle = {
                file: handle
            };
        } else {
            handle = {};
        }
    }

    /**
     * Assign default HTTP status code
     */
    if (!handle.hasOwnProperty("status")) {
        handle.status = 200;
    }

    /**
     * Serve redirect from handler response
     * Handler needs to return status and path
     */
    if (handle.status >= 300 && handle.status < 400) {
        /**
         * Serve redirect, if "path" provided
         */
        if (handle.hasOwnProperty("path")) {
            return module.exports.redirect(
                request,
                response,
                handle.status,
                handle.path
            );
        }
        return module.exports.status(request, response, 500);
    }

    /**
     * Serve data directly from handler response
     * Handler needs to return data and type
     */
    if (handle.hasOwnProperty("data")) {
        return module.exports.data(
            request,
            response,
            handle.status,
            handle.type,
            handle.data
        );
    }

    /**
     * Default to "index.html" if no file provided
     */
    if (!handle.hasOwnProperty("file")) {
        handle.file = "index.html";
    }

    /**
     * If the handler file is outside of "/public"
     * Prefix the handler file with public directory
     */
    if (
        handle.file.substr(0, request.config.base.length) !==
        request.config.base
    ) {
        handle.file =
            request.url.file.substr(0, request.url.file.lastIndexOf("/") + 1) +
            handle.file;
    }

    /**
     * Attempt to serve the new file route
     */
    request.url.file = handle.file;
    return module.exports.file(request, response, handle.status);
}

module.exports = {
    /**
     * Redirect the client to another page
     *
     * @param {Object} res Instance of HTTP.ServerResponse
     * @param {number|string} code HTTP status code (3xx)
     * @param {string} path Path to redirect
     */
    redirect(request, response, code, path) {
        "use strict";

        /**
         * Default "code" to 307
         */
        if (code === undefined) {
            code = 307;
        }

        request.output("request", { request });
        request.url.pathname = path;
        request.output("redirect", { request, response, status: code });

        response.writeHead(code, {
            Location: path
        });
        return response.end();
    },

    /**
     *
     * @param {Object} request Instance of HTTP.IncomingMessage
     * @param {Object} response Instance of HTTP.ServerResponse
     * @param {Number} status HTTP Status code for request
     * @param {Boolean} index_only Whether to use only index.json files
     */
    directory(request, response, status = 200, index_only = false) {
        request.output("directory", {
            request: request,
            response,
            status,
            index_only
        });

        /**
         * Stop checking for files if we've reached the base of the server
         */
        if (
            request.url.file.substr(0, request.config.base.length) !==
            request.config.base
        ) {
            return module.exports.status(request, response, 404);
        }

        /**
         * Log that we're checking this directory for files
         */
        request.log.push(request.url.file.substr(request.cwd_l));
        if (!request.log[request.log.length - 1].length) {
            request.log[request.log.length - 1] += path.sep;
        }

        /**
         * If the request is a file, redirect to "file" function handler
         */
        if (
            request.url.file
                .substr(request.url.file.lastIndexOf(path.sep))
                .indexOf(".") > -1
        ) {
            return module.exports.file(request, response, 200);
        }

        /**
         * Strip off the end of the URL to check the entire directory
         * This prepares the directory path for the next shorten
         */
        if (request.url.file.substr(-1) === path.sep) {
            request.url.file = request.url.file.substr(
                0,
                request.url.file.lastIndexOf(path.sep)
            );
        }

        /**
         * Scan the current directory for all files
         */
        return fs.readdir(request.url.file, "utf8", function(error, files) {
            /**
             * If there is an error, we set files to an empty array
             */
            if (error) {
                files = [];
            }

            /**
             * Retrieve the index.json file if it exists & "index" flag enabled
             * This doesn't parse the paths, just the JSON file (for flags)
             */
            return route
                .getIndex(
                    request.url.file,
                    files.indexOf("index.json") > -1 &&
                        request.config.hasOwnProperty("flags") &&
                        typeof request.config.flags === "object" &&
                        request.config.flags.hasOwnProperty("index") &&
                        request.config.flags.index
                )
                .then(function(paths) {
                    /**
                     * Combine pre-existing flags with new index.json flags
                     */
                    if (
                        typeof paths === "object" &&
                        paths.hasOwnProperty("flags")
                    ) {
                        request.config.flags = Object.assign(
                            request.config.flags,
                            paths.flags
                        );
                    }

                    if (
                        !index_only &&
                        (files.indexOf("index.js") > -1 ||
                            files.indexOf("index.html") > -1)
                    ) {
                        /**
                         * If the directory contains an index, append this to the file
                         * We then serve the request into the "file" function
                         */
                        if (files.indexOf("index.js") > -1) {
                            request.url.file += "index.js";
                        } else {
                            request.url.file += "index.html";
                        }
                        return module.exports.file(
                            request,
                            response,
                            status,
                            true
                        );
                    }

                    /**
                     * If set to "index" mode, only check for index.json files
                     * We don't want to serve index.js/index.html unless directed
                     */
                    if (
                        request.config.flags.hasOwnProperty("index") &&
                        request.config.flags.index &&
                        files.indexOf("index.json") > -1
                    ) {
                        /**
                         * If the route could be parsed, redirect to "dir"
                         */
                        let config = route.parse(request, paths);
                        if (config.hasOwnProperty("path") && config.path) {
                            return module.exports.directory(
                                request,
                                response,
                                status
                            );
                        }

                        if (config.hasOwnProperty("file") && config.file) {
                            return module.exports.file(
                                request,
                                response,
                                status,
                                true
                            );
                        }

                        /**
                         * If the route could not be parsed, check config
                         */
                        if (
                            config.hasOwnProperty("flags") &&
                            typeof config.flags === "object"
                        ) {
                            request.config.flags = Object.assign(
                                request.config.flags,
                                config.flags
                            );
                            if (
                                request.config.flags.hasOwnProperty("browse") &&
                                request.config.flags.browse
                            ) {
                                return module.exports.returnBrowse(
                                    request,
                                    response
                                );
                            }
                        }

                        /**
                         * If the route could not be parsed, return HTTP 404
                         */
                        return module.exports.status(request, response, 404);
                    }

                    /**
                     * Strip the end of the URL, then recursively call this function
                     */
                    request.url.file = request.url.file.substr(
                        0,
                        request.url.file.lastIndexOf("/")
                    );
                    return module.exports.directory(
                        request,
                        response,
                        status,
                        true
                    );
                })
                .catch(function(error) {
                    console.log(error);
                    return module.exports.status(request, response, 500);
                });
        });
    },

    /**
     * Return a file to the client
     *
     * @param {Object} request Instance of HTTP.IncomingMessage
     * @param {Object} response Instance of HTTP.ServerResponse
     * @param {Number} [status=200] HTTP Status code for request
     * @param {Boolean} [force=false] Force index of .js file
     */
    file(request, response, status = 200, force = false) {
        request.output("file", { request, response, status, force });

        /**
         * If we've already tried to serve this file before,
         * Don't try to serve the file again - instead return 404.
         */
        if (request.log.indexOf(status + " " + request.url.file) > -1) {
            return module.exports.status(request, response, 404);
        }
        request.log.push(request.url.file.substr(request.cwd_l) + " " + status);

        /**
         * Log that we're trying to serve this file to the console
         */

        /**
         * Lookup the stats for the file requested (does the file exist)
         */
        return fs.stat(request.url.file, function(error, stats) {
            if (error) {
                /**
                 * If we're missing is the 404 file, this is an issue
                 * The final argument of "true" is a creation request
                 */
                if (
                    request.url.file ===
                    request.config.base.replace(/\\/g, "/") + "/status/404.html"
                ) {
                    return module.exports.status(request, response, 404);
                }

                /**
                 * If we're missing the file, begin routing with "dir" function
                 * This is to check for an index.json routing file (htaccess)
                 */
                request.url.file = request.url.file.substr(
                    0,
                    request.url.file.lastIndexOf("/")
                );
                return module.exports.directory(request, response, 200, true);
            }

            /**
             * If the end of the request file is a system file (routers)
             * We don't allow the client to serve these (back-end snooping)
             */
            let base = request.url.file.substr(
                request.url.file.lastIndexOf(path.sep) + 1
            );
            if (base === "index.json" || (force && base.substr(-3) === ".js")) {
                if (base.substr(-3) === ".js") {
                    /**
                     * Delete the require cache before requiring the system file
                     * You don't have to restart the server when changing files
                     *
                     * We assign this to the "base" variable, as this is unused
                     */
                    delete require.cache[require.resolve(request.url.file)];
                    base = require(request.url.file);
                    if (typeof base === "function") {
                        return indexHandler(
                            request,
                            response,
                            base({
                                connection: request.connection,
                                version: request.httpVersion,
                                headers: request.headers,
                                trailers: request.trailers,
                                url: request.url,
                                method: request.method,
                                data: request.data,
                                config: request.config,
                                extra: request.extra
                            })
                        );
                    }
                    return indexHandler(request, response, base);
                }

                /**
                 * If the requested file is unavailable (or index.json)
                 * We instead return a 500 as the request cannot be handled
                 */
                return module.exports.status(request, response, 403);
            }

            /**
             * If the file does exist, we read the content from the file
             * This only happens if SSI is enabled, otherwise it is streamed
             * We don't specify an encoding, as this may be an image/other
             */
            if (
                (request.config.flags.hasOwnProperty("ssi") &&
                    request.config.flags.ssi &&
                    request.url.file.substr(-5).indexOf(".htm") > -1) ||
                request.url.file.substr(-3) === ".js"
            ) {
                return fs.readFile(request.url.file, function(error, data) {
                    /**
                     * If we have an error reading the file, return a 404
                     */
                    if (error) {
                        return module.exports.status(request, response, 404);
                    }

                    /**
                     * If trying to read a server module file, return a 404
                     */
                    let data_str = data.toString();
                    if (
                        data_str.indexOf("module.exports = ") > -1 &&
                        data_str.indexOf("window.") == -1 &&
                        data_str.indexOf("typeof window") == -1
                    ) {
                        return module.exports.status(request, response, 403);
                    }

                    /**
                     * Serrve the request using the new data from the file system
                     * fs.stat() is used to retrieve the modified time for the file
                     */
                    return module.exports.data(
                        request,
                        response,
                        status,
                        mime.lookup(request.url.file),
                        data,
                        new Date(stats.mtime).toUTCString()
                    );
                });
            }

            /**
             * Stream the file
             */
            response.writeHead(status);
            return fs.createReadStream(request.url.file).pipe(response);
        });
    },

    /**
     *
     * @param {Object} request Instance of HTTP.IncomingMessage
     * @param {Object} response Instance of HTTP.ServerResponse
     * @param {Number} code HTTP Status code for request
     * @param {Boolean} [browse=true] Whether to allow server browsing
     */
    status(request, response, code, browse = true) {
        request.output("status", { request, response, status: code, browse });

        /**
         * Prepare the status file location
         */
        // request.url.file = request.config.base + "/status/" + code + ".html";
        request.url.file = path.join(
            request.config.base,
            "status",
            code + ".html"
        );

        /**
         * Replace 404 with browse if flag enabled
         */
        if (
            parseInt(code) === 404 &&
            request.config.hasOwnProperty("flags") &&
            typeof request.config.flags === "object" &&
            request.config.flags.hasOwnProperty("browse") &&
            request.config.flags.browse &&
            browse
        ) {
            return module.exports.returnBrowse(request, response);
        }

        return fs.stat(request.url.file, function(error) {
            /**
             * If the status file doesn't exist, check the directory
             */
            if (error) {
                return fs.stat(
                    path.join(request.config.base, "status"),
                    function(error) {
                        if (error) {
                            return fs.stat(request.config.base, function(
                                error
                            ) {
                                if (error) {
                                    return fs.mkdir(
                                        request.config.base,
                                        function(error) {
                                            if (error) {
                                                console.log(error);
                                            }

                                            return module.exports.status(
                                                request,
                                                response,
                                                code
                                            );
                                        }
                                    );
                                }

                                /**
                                 * Create the status directory if needed
                                 */
                                return fs.mkdir(
                                    path.join(request.config.base, "status"),
                                    function(error) {
                                        if (error) {
                                            console.log(error);
                                        }
                                        return module.exports.status(
                                            request,
                                            response,
                                            code
                                        );
                                    }
                                );
                            });
                        }

                        /**
                         * Create the status page if needed
                         */
                        return fs.writeFile(
                            request.url.file,
                            code,
                            "utf8",
                            function(error) {
                                if (error) {
                                    console.log(error);
                                }
                                return module.exports.status(
                                    request,
                                    response,
                                    code
                                );
                            }
                        );
                    }
                );
            }

            /**
             * Serve the requested status page
             */
            return module.exports.file(request, response, code);
        });
    },

    /**
     * Return data to the client
     *
     * @param {Object} request Instance of HTTP.IncomingMessage
     * @param {Object} response Instance of HTTP.ServerResponse
     * @param {Number} [status=200] HTTP Status code for request
     * @param {String} type Content-Type for the data
     * @param {String} data Data to return to client
     * @param {String} modified Last-Modified for the data
     */
    data(
        request,
        response,
        status = 200,
        type = undefined,
        data = undefined,
        modified = undefined
    ) {
        request.output("data", {
            request,
            response,
            status,
            type,
            data,
            modified
        });

        /**
         * Pass the provided "data" into ssi.process
         * This will return all server side replacements
         */
        return ssi
            .process(
                request,
                data,
                Object.assign(
                    {},
                    request.headers,
                    request.url,
                    Object.assign(
                        {},
                        request.data.get,
                        request.data[request.method]
                    )
                ),
                request.config.flags.hasOwnProperty("ssi") &&
                    request.config.flags.ssi
            )
            .then(function(data) {
                /**
                 * Respond with the provided content type/data
                 * Assigns the following HTTP headers:
                 * - Accept-Ranges ("bytes")
                 * - Content-Type ("type" variable)
                 * - Content-Length (Buffer.byteLength of "data" variable)
                 * - Last-Modified ("modified" variable, or current time)
                 */
                let headers = {
                    "Last-Modified": modified || new Date().toUTCString()
                };
                if (typeof type === "string") {
                    headers["Content-Type"] = type;
                }

                /**
                 * Ensure "data" is typeof string, or Buffer
                 */
                if (!(typeof data === "string" || Buffer.isBuffer(data))) {
                    data = JSON.stringify(data);
                    headers["Content-Type"] = "application/json";
                }

                response.writeHead(status, headers);
                return response.end(data);
            })
            .catch(function(error) {
                console.log(error);
            });
    }
};
