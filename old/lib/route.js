/*jslint this single long white node*/

/**
 * Require Node.js core modules
 * fs - for utilising the server file system (https://nodejs.org/api/fs.html)
 */
const fs = require("fs");

/**
 * Regex for finding groups in string
 */
const replaceGroups_regex = new RegExp("\\$(\\d+)", "g");

module.exports = {
    /**
     * Replace regex group placeholders in string
     *
     * @param {string} string
     * @param {Array} variables
     */
    replaceGroups(string, variables) {
        "use strict";

        /**
         * Extract regex group placeholders until there are none
         * "replaceGroups_regex" pattern is looking for: $1, $2, etc.
         */
        let match = replaceGroups_regex.exec(string);
        while (match !== null) {
            match[0] = new RegExp(match[0].replace("$", "\\$"), "g");

            /**
             * Replace all regex group placeholders with value
             * If an invalid number is given, an empty string is used
             */
            if (!variables.hasOwnProperty(match[1])) {
                variables[match[1]] = "";
            }
            string = string.replace(match[0], variables[match[1]]);

            match = replaceGroups_regex.exec(string);
        }
        return string;
    },

    /**
     * Parse "index.json" file, modify "req" object
     *
     * @param {Object} req Instance of http.ClientRequest
     * @param {string|Object} paths JSON "index.json" paths
     */
    parse(req, paths) {
        "use strict";

        /**
         * Parse JSON file into object, if not already object
         * Each key in the object should be a regex string
         */
        if (typeof paths !== "object") {
            try {
                paths = JSON.parse(paths);
            } catch (Error) {
                console.log(Error);
            }
        }

        /**
         * Directory config, for if no path used
         */
        let config = {};
        if (paths.hasOwnProperty("flags")) {
            config.flags = paths.flags;
            delete paths.flags;
        }

        /**
         * req.url.diff is the difference of request URL (origin) and this file
         */
        req.url.diff = req.url.origin.substr(req.url.file.length + 1);

        /**
         * Loop through each route until one matches, then parse
         * Returns true if a route was parsed, otherwise false
         */
        let Regex;
        Object.keys(paths).some(function(path) {
            /**
             * Create regex from object key to check with path
             * Execute the regex against the diff url
             */
            Regex = new RegExp(path, paths[path].flags);
            Regex = Regex.exec(req.url.diff);

            /**
             * If there is a regex match (full match)
             * So the regex doesn't have to include ^ or $
             */
            if (Regex !== null && Regex[0] === req.url.diff) {
                config = Object.assign(config, {
                    path: true
                });

                if (paths[path].hasOwnProperty("query")) {
                    /**
                     * Loop each query in the route, replace group placeholders
                     * Uses the "replaceGroups" function to allow URL rewrites
                     */
                    Object.keys(paths[path].query).forEach(function(prop) {
                        req.data.get[
                            module.exports.replaceGroups(prop, Regex)
                        ] = module.exports.replaceGroups(
                            paths[path].query[prop],
                            Regex
                        );
                    });
                }

                /**
                 * Append the "file" from route to the end of the current file
                 * Path doesn't have to include a slash as they're all appended
                 */
                req.url.file += "/";
                if (paths[path].hasOwnProperty("file")) {
                    req.url.file += module.exports.replaceGroups(
                        paths[path].file,
                        Regex
                    );

                    delete config.path;
                    config.file = true;
                }

                return true;
            }

            return false;
        });

        return config;
    },
    /**
     * Retrieve and pre-parse an index.json file
     *
     * @param {Sring} directory Directory location of the index.json file
     * @param {Boolean} index Whether index checking is enaled
     */
    getIndex(directory, index) {
        "use strict";

        return new Promise(function(resolve, reject) {
            /**
             * Reject if no directory
             */
            if (!directory) {
                return reject("Missing directory");
            }

            /**
             * Return immediately if no index exists
             */
            index = index || false;
            if (index) {
                return fs.readFile(directory + "/index.json", "utf8", function(
                    error,
                    paths
                ) {
                    if (error) {
                        return reject(error);
                    }

                    /**
                     * Parse JSON file into object, if not already object
                     * Each key in the object should be a regex string
                     */
                    try {
                        return resolve(JSON.parse(paths));
                    } catch (Error) {
                        if (typeof Error === "object") {
                            return resolve({});
                        }
                        console.log(Error);
                    }
                });
            }

            return resolve({});
        });
    }
};
