/*jslint white node*/

/**
 * Require Node.js core modules
 * fs - for utilising the server file system (https://nodejs.org/api/fs.html)
 */
const fs = require("fs");

/**
 * Require NPM packages
 * ip - for IP addresses in mutli-site suport (https://www.npmjs.com/package/ip)
 */
const ip = require("ip");

module.exports = {
    /**
     * Establish the default site config
     */
    default: {
        site: {
            directory: "public"
        }
    },

    /**
     * Retrieve the current site for the request given
     *
     * @param {Object} req Instance of HTTP.ClientRequest
     * @param {Object} config Site config settings/previous caches
     */
    getSite(req, config) {
        "use strict";

        return new Promise(function(resolve) {
            /**
             * Regex for normalizing property names
             * Removes - or _ from property when checking
             */
            let spacers = new RegExp("[-_]", "g");

            /**
             * Build the request object, for caching
             * Contains all selectable properties for sites.json
             */
            let request = {
                domain: req.url.hostname,
                clientip: req.connection.remoteAddress,
                serverip: ip.address(),
                path: req.url.pathname
            };
            request.cache = JSON.stringify(request);

            /**
             * Set the IP to forwarded for, if header provided
             */
            if (
                req.headers.hasOwnProperty("x-forwarded-for") &&
                req.headers["x-forwarded-for"].length
            ) {
                request.clientip = req.headers["x-forwarded-for"];
            }

            /**
             * Use request from cache (if available)
             */
            if (
                config.hasOwnProperty("cache") &&
                typeof config.cache === "object" &&
                config.cache.hasOwnProperty(request.cache)
            ) {
                return resolve({
                    site: config.cache[request.cache],
                    config: config
                });
            }

            let site;
            let match = false;
            let match_regex;

            /**
             * Default to using "public" directory, if none found
             */
            if (!Object.keys(config).length) {
                return resolve({
                    site: module.exports.default.site,
                    config: config
                });
            }

            if (
                Object.keys(config).some(function(condition) {
                    /**
                     * Split property from the regex value (default to "domain")
                     */
                    site = condition.split("|");
                    if (site.length === 1) {
                        site = ["domain", site[0]];
                    }

                    /**
                     * Process only if the regex value has a length above 0
                     */
                    if (site[1].length) {
                        /**
                         * Convert the regex section into a RegExp ("i" flag)
                         */
                        match_regex = new RegExp(site[1], "i");

                        /**
                         * Replace certain characters to standardize types
                         */
                        site[0] = site[0].replace(spacers, "").toLowerCase();
                        if (site[0].substr(0, 2) === "ip") {
                            site[0] = site[0].substr(2) + "ip";
                        }

                        /**
                         * Execute the regex match against the request
                         */
                        if (request.hasOwnProperty(site[0])) {
                            match = match_regex.exec(request[site[0]]);
                            if (
                                match !== null &&
                                match[0] === request[site[0]]
                            ) {
                                match = true;
                            } else if (site[1].substr(-2) === "ip") {
                                /**
                                 * If the regex failed, check the IP for cidr
                                 */
                                match = ip
                                    .cidrSubnet(site[1])
                                    .contains(request[site[0]]);
                            } else {
                                match = false;
                            }

                            if (match) {
                                match = config[condition];
                            } else {
                                match = false;
                            }
                        }
                    }

                    return match !== false;
                })
            ) {
                /**
                 * Standardize/normalize site config
                 */
                if (match.hasOwnProperty("directory")) {
                    if (match.directory.substr(0, 1) === "/") {
                        match.directory = match.directory.substr(1);
                    }
                    if (match.directory.substr(-1) === "/") {
                        match.directory = match.directory.substr(
                            0,
                            match.directory.length - 1
                        );
                    }
                } else {
                    match.directory = "public";
                }

                /**
                 * Establish a cache (if not already found)
                 */
                if (!config.hasOwnProperty("cache")) {
                    config.cache = {};
                }

                /**
                 * Add request to cache
                 */
                config.cache[JSON.stringify(request)] = match;

                return resolve({
                    site: match,
                    config: config
                });
            }

            return resolve({
                site: module.exports.default.site,
                config: config
            });
        });
    },
    getConfig(base) {
        "use strict";

        return new Promise(function(resolve, reject) {
            /**
             * Retrieve config information from sites.json in base
             */
            return fs.readFile(base + "sites.json", function(error, sites) {
                if (error) {
                    sites = "{}";
                }

                try {
                    return resolve(JSON.parse(sites));
                } catch (ignore) {
                    return reject("Unable to parse 'config.json'");
                }
            });
        });
    }
};
