/*jslint single long white node*/

/**
 * Require Node.js core modules
 * fs - for read/write to the file system (https://nodejs.org/api/fs.html)
 * path - for resolving relative links (https://nodejs.org/api/path.html)
 * child_process - for SSI execution (https://nodejs.org/api/child_process.html)
 */
const fs = require("fs");
const path = require("path");
const child_process = require("child_process");

/**
 * Convert relative path into absolute string
 * Allows optional parameter "directory" for targeting
 *
 * Maintains "public" directory within the path
 *
 * @param {String} file File string to make absolute
 * @param {String|boolean} directory Directory to be relative from
 */
function relativeTo(parent, file, directory) {
    "use strict";

    /**
     * If we have been given a directory, set relative to this
     * Prefix with "./" if not already a relative path
     */
    if (directory) {
        if (file.substr(0, 1) !== ".") {
            file = "./" + file;
        }
        return path.resolve(directory, file);
    }

    /**
     * Ensure the path is not anchored on directory tree
     * Then resolve to absolute with path module
     */
    if (file.substr(0, 1) === "/") {
        file = file.substr(1);
    }
    return path.resolve(parent, file);
}

module.exports = {
    /**
     * Allow server-side includes and other commands (SSI)
     *
     * @param {string} data Data to check for SSI comments
     * @param {Object} variables Variables to use in <!--#echo var="name" -->
     */
    process(request, data, variables, enable) {
        "use strict";

        /**
         * RegExp for finding SSI comments, syntax:
         * <!--#[command] <variable="value"> -->
         */
        const process_regex = new RegExp(
            '<!--#(include|exec|echo|config|f([\\w]+)|printenv) ((\\w+)="(.+)" )?-->'
        );

        /**
         * Remove end file from origin, if exists
         */
        let origin = request.url.file;
        if (origin.substr(origin.lastIndexOf("/")).indexOf(".") > -1) {
            origin = origin.substr(0, origin.lastIndexOf("/"));
        }

        return new Promise(function(resolve, reject) {
            /**
             * Skip SSI if SSI has been disabled
             */
            if (!enable) {
                return resolve(data);
            }

            /**
             * Execute the regex on the data, finding any comments
             */
            let match = process_regex.exec(data);
            if (match !== null) {
                /**
                 * Create a new RegExp instance of this comment as global
                 * This will be used to replace any duplicate uses
                 */
                match[0] = new RegExp(match[0], "g");

                /**
                 * If the match begins with "f" we treat this as a stat command
                 * In Apache, commands flastmod and fsize are supported
                 * In this server, we allow any fs.stat return property
                 */
                if (match[1].substr(0, 1) === "f") {
                    /**
                     * Trim the "f" from the front of the stat property
                     */
                    match[1] = match[1].substr(1);

                    /**
                     * Ensure the file to stat is absolutely positioned
                     */
                    match[5] = relativeTo(
                        request.config.base,
                        match[5],
                        match[4] === "file" ? origin : false
                    ).replace(/\\/g, "/");

                    /**
                     * Retrieve the stats for the specified file
                     */
                    return fs.stat(match[5], function(error, stats) {
                        /**
                         * If we do not find a file (or any other error)
                         * Reset the stat to "" so we can continue with our code
                         * Sets stats to {} as regular variable is undefined
                         */
                        if (error) {
                            match[1] = "";
                            stats = {};
                        }

                        /**
                         * Convert "lastmod" into "mtime" to support Apache
                         */
                        if (match[1] === "lastmod") {
                            match[1] = "mtime";
                        }

                        /**
                         * Insert a placeholder value in the Object, if missing
                         * This makes it cleaner than using a ternary operator
                         */
                        if (!stats.hasOwnProperty(match[1])) {
                            stats[match[1]] = "";
                        }

                        /**
                         * Replace the comment with the stat specified
                         */
                        return module.exports
                            .process(
                                request,
                                data
                                    .toString()
                                    .replace(match[0], stats[match[1]]),
                                variables,
                                true
                            )
                            .then(function(data) {
                                return resolve(data);
                            })
                            .catch(function(error) {
                                return reject(error);
                            });
                    });
                }

                /**
                 * If the command does not begin with "f" we look for full names
                 * The full list of supported commands are:
                 *  - include (for performing sub-files)
                 *  - exec (for executing cgi/cmd scripts)
                 *  - echo (for echoing variables)
                 *  - printenv (for listing available echo variables)
                 */
                switch (match[1]) {
                    case "include":
                        /**
                         * Ensure the target include file is absolute position
                         */
                        match[5] = relativeTo(
                            request.config.base,
                            match[5],
                            match[4] === "file" ? origin : false
                        ).replace(/\\/g, "/");

                        request.output("ssi include", {
                            request,
                            data,
                            variables,
                            enable,
                            match
                        });

                        /**
                         * Read the data from the file for the include
                         * We don't need fs.stat first, as error catches this
                         */
                        return fs.readFile(match[5], function(error, include) {
                            /**
                             * If we're unable to read/find file, use ""
                             */
                            if (error) {
                                include = "";
                            }

                            /**
                             * Replace the comment string with the matched data
                             * Then call process again to keep executing
                             */
                            return module.exports
                                .process(
                                    request,
                                    data.toString().replace(match[0], include),
                                    variables,
                                    true
                                )
                                .then(function(data) {
                                    return resolve(data);
                                })
                                .catch(function(error) {
                                    return reject(error);
                                });
                        });

                    case "exec":
                        if (match[4] === "cgi") {
                            /**
                             * Execute child process for CGI file
                             * (currently not working, unsure why)
                             */
                            try {
                                return child_process.execFile(
                                    relativeTo(request.config.base, match[5]),
                                    function(error, stdout, stderr) {
                                        error = error || stderr;
                                        if (error) {
                                            throw error;
                                        }

                                        return module.exports
                                            .process(
                                                request,
                                                data
                                                    .toString()
                                                    .replace(match[0], stdout),
                                                variables,
                                                true
                                            )
                                            .then(function(data) {
                                                return resolve(data);
                                            })
                                            .catch(function(error) {
                                                return reject(error);
                                            });
                                    }
                                );
                            } catch (error) {
                                console.log(error);

                                return module.exports
                                    .process(
                                        request,
                                        data.toString().replace(match[0], ""),
                                        variables,
                                        true
                                    )
                                    .then(function(data) {
                                        return resolve(data);
                                    })
                                    .catch(function(error) {
                                        return reject(error);
                                    });
                            }
                        }

                        if (match[4] === "cmd") {
                            /**
                             * Execute child process for CMD commands
                             */
                            if (match[5].substr(-4) === ".cmd") {
                                match[5] = relativeTo(
                                    request.config.base,
                                    match[5]
                                ).replace(/\\/g, "/");

                                request.output("ssi cmd", {
                                    request,
                                    data,
                                    variables,
                                    enable,
                                    match
                                });
                            }

                            return child_process.exec(match[5], function(
                                error,
                                stdout,
                                stderr
                            ) {
                                /**
                                 * Both errors (NodeJS and CMD) handled together
                                 * Rather than terminating, replace with ""
                                 */
                                error = error || stderr;
                                if (error) {
                                    return module.exports
                                        .process(
                                            request,
                                            data
                                                .toString()
                                                .replace(match[0], ""),
                                            variables,
                                            true
                                        )
                                        .then(function(data) {
                                            return resolve(data);
                                        })
                                        .catch(function(error) {
                                            return reject(error);
                                        });
                                }

                                /**
                                 * Replace the comment with the stdout result
                                 */
                                return module.exports
                                    .process(
                                        request,
                                        data
                                            .toString()
                                            .replace(match[0], stdout),
                                        variables,
                                        true
                                    )
                                    .then(function(data) {
                                        return resolve(data);
                                    })
                                    .catch(function(error) {
                                        return reject(error);
                                    });
                            });
                        }
                        break;

                    case "echo":
                    case "printenv":
                        /**
                         * Convert variables to lowercase
                         */
                        Object.keys(variables).forEach(function(property) {
                            if (typeof variables[property] === "string") {
                                variables[property.toLowerCase()] = variables[
                                    property
                                ].toLowerCase();
                            }
                            if (variables[property] === null) {
                                delete variables[property];
                            }
                        });

                        /**
                         * If we're using an echo, we stringify non-strings
                         * This also ensures no rogue properties are requested
                         */
                        if (match[1] === "echo") {
                            match[5] = match[5].toLowerCase();
                            if (variables.hasOwnProperty(match[5])) {
                                if (typeof variables[match[5]] === "object") {
                                    variables[match[5]] = JSON.stringify(
                                        variables[match[5]]
                                    );
                                }
                            } else {
                                variables[match[5]] = "";
                            }
                        }

                        /**
                         * Replace the comment with single/list of variables
                         */
                        return module.exports
                            .process(
                                request,
                                data
                                    .toString()
                                    .replace(
                                        match[0],
                                        match[1] === "echo"
                                            ? variables[match[5]]
                                            : JSON.stringify(variables)
                                    ),
                                variables,
                                true
                            )
                            .then(function(data) {
                                return resolve(data);
                            })
                            .catch(function(error) {
                                return reject(error);
                            });

                    default:
                        /**
                         * By default we just strip the comment from the data
                         */
                        return module.exports
                            .process(
                                request,
                                data.toString().replace(match[0], ""),
                                variables,
                                true
                            )
                            .then(function(data) {
                                return resolve(data);
                            })
                            .catch(function(error) {
                                return reject(error);
                            });
                }
            }

            return resolve(data);
        });
    }
};
