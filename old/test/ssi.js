/*jslint long white node browser*/
/*global it describe*/

/**
 * Require Node.js core modules
 * fs - for utilising the server file system (https://nodejs.org/api/fs.html)
 * http - for operating the HTTP Web Server (https://nodejs.org/api/http.html)
 * assert - for unit testing package code (https://nodejs.org/api/assert.html)
 */
const fs = require("fs");
const http = require("http");
const assert = require("assert");

/**
 * Require internal modules
 * JSiteServer - for running the jsite-server package
 * utils - for testing jsite modules
 */
const JSiteServer = require("..");
const utils = require("./lib/utils");

/**
 * Whether to output server log
 */
let output = false;

function getTest(Server, method, params) {
    "use strict";

    if (params) {
        params = ` (${params})`;
    } else {
        params = ``;
    }

    let test = "Should ";
    if (!Server.config.flags.ssi) {
        test += "not ";
    }
    test += `replace #${method}${params}`;
    return test;
}

function handleDirectory(Server, method, port, url, expected) {
    "use strict";

    if (url.substr(-1) !== "/") {
        url += "/";
    }

    it(getTest(Server, method, "virtual"), function(done) {
        return utils.testGET(port, url + "current.html", expected, done);
    });
    it(getTest(Server, method, "virtual, slash"), function(done) {
        return utils.testGET(port, url + "current_slash.html", expected, done);
    });
    it(getTest(Server, method, "virtual, sub"), function(done) {
        return utils.testGET(port, url + "sub.html", expected, done);
    });
    it(getTest(Server, method, "virtual, sub, slash"), function(done) {
        return utils.testGET(port, url + "sub_slash.html", expected, done);
    });
    it(getTest(Server, method, "virtual, parent"), function(done) {
        return utils.testGET(port, url + "parent.html", expected, done);
    });
    it(getTest(Server, method, "virtual, parent, slash"), function(done) {
        return utils.testGET(port, url + "parent_slash.html", expected, done);
    });
}

function testSSI(Server, port) {
    "use strict";

    /**
     * Testing Server Side Includes
     */
    describe(`Server Side Includes - Testing (${port})`, function() {
        // #include
        handleDirectory(Server, "include", port, "ssi/include/file", function(
            data
        ) {
            if (Server.config.flags.ssi) {
                return data === "include";
            }
            return (
                data.substr(0, 13) === "<!--#include " &&
                data.substr(-4) === " -->"
            );
        });
        handleDirectory(
            Server,
            "include",
            port,
            "ssi/include/virtual",
            function(data) {
                if (Server.config.flags.ssi) {
                    return data === "include";
                }
                return (
                    data.substr(0, 13) === "<!--#include " &&
                    data.substr(-4) === " -->"
                );
            }
        );

        // #exec
        it(getTest(Server, "exec"), function(done) {
            return utils.testGET(
                port,
                "ssi/exec/exec.html",
                function(data) {
                    if (Server.config.flags.ssi) {
                        return data === "test";
                    }
                    return (
                        data.substr(0, 10) === "<!--#exec " &&
                        data.substr(-4) === " -->"
                    );
                },
                done
            );
        });
        handleDirectory(Server, "exec", port, "ssi/exec/virtual", function(
            data
        ) {
            if (Server.config.flags.ssi) {
                return data === "test";
            }
            return (
                data.substr(0, 10) === "<!--#exec " &&
                data.substr(-4) === " -->"
            );
        });

        // #echo
        it(getTest(Server, "echo"), function(done) {
            return utils.testGET(
                port,
                "ssi/echo.html",
                function(data) {
                    if (Server.config.flags.ssi) {
                        return data === "/ssi/echo.html";
                    }
                    return (
                        data.substr(0, 10) === "<!--#echo " &&
                        data.substr(-4) === " -->"
                    );
                },
                done
            );
        });

        // #fsize
        it(getTest(Server, "fsize"), function(done) {
            return fs.stat(__dirname + "/public/ssi/file.html", function(
                error,
                stats
            ) {
                if (error) {
                    assert.fail(error);
                    done();
                }
                return utils.testGET(
                    port,
                    "ssi/fsize.html",
                    function(data) {
                        if (Server.config.flags.ssi) {
                            return parseInt(data) === stats.size;
                        }
                        return (
                            data.substr(0, 11) === "<!--#fsize " &&
                            data.substr(-4) === " -->"
                        );
                    },
                    done
                );
            });
        });

        // #printenv
        it(getTest(Server, "printenv"), function(done) {
            return utils.testGET(
                port,
                "ssi/printenv.html",
                function(data) {
                    if (Server.config.flags.ssi) {
                        return typeof JSON.parse(data) === "object";
                    }
                    return (
                        data.substr(0, 14) === "<!--#printenv " &&
                        data.substr(-4) === " -->"
                    );
                },
                done
            );
        });
    });
}

let TestServer = new JSiteServer({
    output,
    flags: {
        ssi: true
    }
});

describe("Server Side Includes - Startup (80)", function() {
    "use strict";

    it("Should listen on port 80", function(done) {
        TestServer.listen(function() {
            let req = http.get("http://localhost:80", function(response) {
                let data = "";
                response
                    .on("data", function(buffer) {
                        data += buffer;
                    })
                    .on("end", function() {
                        assert.equal(response.statusCode, 200);

                        testSSI(TestServer, 80, done);

                        describe("Server Side Includes - Shutdown (80)", function() {
                            it("Should shut the server down", function(done) {
                                TestServer.close(function() {
                                    done();
                                });
                            });
                        });

                        done();
                    });
            });

            req.on("error", function() {
                assert.equal(true, false);
                TestServer.close();
                done();
            });
        });
    });
});

let TestServer2 = new JSiteServer({
    output,
    flags: {
        ssi: false
    }
});

let test_port = 8080;

describe(`Server Side Includes - Startup (${test_port})`, function() {
    "use strict";

    it(`Should listen on port ${test_port}`, function(done) {
        TestServer2.listen({
            port: test_port,
            callback: function() {
                return utils.testGET(
                    test_port,
                    "",
                    function(ignore, res) {
                        return res.statusCode === 200;
                    },
                    done
                );
            }
        });

        testSSI(TestServer2, test_port, done);

        describe(`Server Side Includes - Shutdown (${test_port})`, function() {
            it("Should shut the server down", function(done) {
                TestServer2.close(done);
            });
        });
    });
});
