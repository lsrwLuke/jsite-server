module.exports = function(request) {
    let result = {
        data: 1
    };

    try {
        if (request.data.get.hasOwnProperty("foo")) {
            result.data += 1;
        }

        return result;
    } catch (error) {
        result.data = 0;
        return result;
    }
};
