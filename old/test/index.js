/*jslint long white node browser*/
/*global it describe*/

/**
 * Require Node.js core modules
 * assert - for unit testing package code (https://nodejs.org/api/assert.html)
 */
const assert = require("assert");

/**
 * Require internal modules
 * JSiteServer - for running the jsite-server package
 * utils - for testing jsite modules
 */
const JSiteServer = require("..");
const utils = require("./lib/utils");

/**
 * Whether to output server log
 */
let output = false;

function testIndex(port, Server) {
    "use strict";

    describe(`Index Files - Startup (${port})`, function() {
        it(`Should listen on port (${port})`, function(done) {
            Server.listen({
                port: port,
                callback: function() {
                    describe(`Index Files - Testing (${port})`, function() {
                        it("Should not need an index file", function(done) {
                            utils.testGET(
                                port,
                                "index/",
                                function(data) {
                                    try {
                                        data = JSON.parse(data);
                                        return (
                                            typeof data === "object" &&
                                            data.hasOwnProperty("get") &&
                                            Object.keys(data.get).length === 0
                                        );
                                    } catch (error) {
                                        assert.fail(error);
                                        return false;
                                    }
                                },
                                done
                            );
                        });

                        let test = {
                            prefix: "Should ",
                            json: true
                        };

                        if (
                            Server.config.flags.hasOwnProperty("index") &&
                            !Server.config.flags.index
                        ) {
                            test.prefix += "not ";
                            test.json = false;
                        }

                        it(`${
                            test.prefix
                        }use default index file (${port})`, function(done) {
                            utils.testGET(
                                port,
                                "index/123/",
                                function(data) {
                                    if (test.json) {
                                        try {
                                            data = JSON.parse(data);
                                            return (
                                                typeof data === "object" &&
                                                data.hasOwnProperty("get") &&
                                                Object.keys(data.get).length >
                                                    0 &&
                                                data.get.test === "123"
                                            );
                                        } catch (error) {
                                            assert.fail(error);
                                            return false;
                                        }
                                    }
                                    return data === "404";
                                },
                                done
                            );
                        });

                        it(`${
                            test.prefix
                        }use custom index file (handle) (${port})`, function(done) {
                            utils.testGET(
                                port,
                                "index/handle/123/",
                                function(data) {
                                    if (test.json) {
                                        return data.trim() === "2";
                                    }
                                    return data === "404";
                                },
                                done
                            );
                        });
                    });

                    describe(`Index Files - Restrict Usage (${port})`, function() {
                        it("Should restrict JavaScript file", function(done) {
                            utils.testGET(
                                port,
                                "index/restrict/blocked.js",
                                function(data) {
                                    return data.trim() === "403";
                                },
                                done
                            );
                        });

                        it("Should restrict JavaScript file", function(done) {
                            utils.testGET(
                                port,
                                "index/restrict/not-blocked.js",
                                function(data) {
                                    return data.trim() !== "403";
                                },
                                done
                            );
                        });
                    });

                    describe(`Index Files - Shutdown (${port})`, function() {
                        it("Should shut the server down", function(done) {
                            Server.close(function() {
                                done();
                            });
                        });
                    });

                    done();
                }
            });
        });
    });
}

testIndex(
    81,
    new JSiteServer({
        output,
        flags: {
            index: true
        }
    })
);

testIndex(
    82,
    new JSiteServer({
        output,
        flags: {
            index: false
        }
    })
);
