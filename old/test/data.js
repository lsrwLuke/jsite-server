/*jslint long white node browser*/
/*global it describe*/

/**
 * Require internal modules
 * JSiteServer - for running the jsite-server package
 * utils - for testing jsite modules
 */
const JSiteServer = require("..");
const utils = require("./lib/utils");

/**
 * Whether to output server log
 */
let output = false;

let TestServer = new JSiteServer({
    output,
    flags: {
        index: true
    }
});

let port = 83;

describe(`Request Data - Startup (${port})`, function() {
    "use strict";

    it(`Should listen on port (${port})`, function(done) {
        TestServer.listen({
            port,
            callback: function() {
                describe(`Request Data - Testing (${port})`, function() {
                    it(`Should transfer GET data`, function(done) {
                        utils.testGET(
                            port,
                            "data/object/?foo=1",
                            function(data) {
                                return data !== "0";
                            },
                            done
                        );
                    });

                    it(`Should retrieve GET data`, function(done) {
                        utils.testGET(
                            port,
                            "data/request/?foo=1",
                            function(data) {
                                data = JSON.parse(data);
                                return (
                                    data.data.hasOwnProperty("get") &&
                                    data.data.get.hasOwnProperty("foo") &&
                                    data.data.get.foo === "1"
                                );
                            },
                            done
                        );
                    });
                });

                describe(`Request Data - Shutdown (${port})`, function() {
                    it(`Should shut the server down`, function(done) {
                        TestServer.close(done);
                    });
                });

                done();
            }
        });
    });
});
