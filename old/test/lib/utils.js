const http = require("http");
const assert = require("assert");

module.exports = {
    testGET(port, url, expected, done) {
        "use strict";

        let req = http.get(`http://localhost:${port}/${url}`, function(res) {
            let data = "";

            return res
                .on("data", function(buffer) {
                    data += buffer;
                })
                .on("end", function() {
                    data = data.trim();
                    if (typeof expected === "function") {
                        assert(expected(data, res));
                    } else {
                        assert.equal(data, expected);
                    }
                    done();
                });
        });

        req.on("error", function(error) {
            assert.fail(error);
            done();
        });
    }
};
